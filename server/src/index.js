import { GraphQLServer } from "graphql-yoga";
import knexLib from 'knex';

import SQLConnector from "./connectors/SQLConnector";
import { schema } from './apiSchema';

const knex = knexLib(require('./knexfile')[process.env.NODE_ENV || 'development']);

const sqlConnector = new SQLConnector({ knex });

const server = new GraphQLServer({
  schema,
  context: () => {
    return {
      sql: sqlConnector,
    }
  }
});

server.start({ port: 4000 }, () => console.info(`Server is running on http://localhost:4000`));

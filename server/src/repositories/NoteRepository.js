import uuid from 'uuid';
import loadConnection from "../utils/loadConnection";

const NOTES_TABLE = 'notes';

const NotesRepository = {
    async load(context, args) {
        const notesData = await context.sql.loadConnection(NOTES_TABLE, args);
        return loadConnection(context, NotesRepository.getById, notesData);
    },

    async getById (context, id) {
        const noteData = await context.sql.loadById(NOTES_TABLE, id);

        if (!noteData) {
            return null;
        }

        return ({
            id: noteData.id,
            createdAt: noteData.created_at,
            updatedAt: noteData.updated_at,
            content: noteData.content,
        });
    },

    async create (context, data) {
        const noteData = {
            id: uuid.v4(),
            content: data.content,
        };

        const { id } = await context.sql.insert(NOTES_TABLE, noteData);
        return NotesRepository.getById(context, id);
    },

    async delete (context, id) {
        const deleted = context.sql.deleteById(NOTES_TABLE, id);
        if (deleted) {
            return ({ deleted: true, id });
        }
        return ({ deleted: false, id })
    }
};

export default NotesRepository;
import { inputObjectType, mutationField, objectType, queryField, stringArg, arg } from "nexus";
import NotesRepository from "../repositories/NoteRepository";

const PageInfo = objectType({
    name: 'PageInfo',
    definition(t) {
        t.string("cursor");
        t.boolean("hasNextPage");
    }
});

const DeletedInfo = objectType({
    name: 'DeletedInfo',
    definition(t) {
        t.boolean("deleted");
        t.string("id")
    }
});

export const Note = objectType({
    name: 'Note',
    definition(t) {
        t.string("id", { nullable: false });
        t.string("content");
        t.string("createdAt");
        t.string("updatedAt");
    },
});

export const Notes = objectType({
    name: 'Notes',
    definition(t) {
        t.list.field('nodes', {
            type: Note,
            nullable: false,
        });
        t.field('pageInfo', {
            type: PageInfo,
            nullable: false,
        });
    },
});

export const insertNoteInputType = inputObjectType({
    name: 'InsertNoteInput',
    definition(t) {
        t.string("content", { required: true })
    }
});

export const deleteNoteInputType = inputObjectType({
    name: 'DeleteNoteInput',
    definition(t) {
        t.string("id", { required: true })
    }
});

export const NoteQueryField = queryField('note', {
    type: Note,
    args: { id: stringArg({ required: true }) },
    async resolve(root, args, context) {
        const data = await NotesRepository.getById(context, args.id);
        return data;
    },
});

export const NotesQueryField = queryField('notes', {
    type: Notes,
    async resolve(root, args, context) {
        const data = await NotesRepository.load(context, args);
        return data;
    },
});

export const NoteCreateField = mutationField("createNote", {
    type: "Note",
    args: {
        input: arg({
            type: insertNoteInputType,
            required: true,
        }),
    },
    resolve (root, args, context) {
        return NotesRepository.create(context, args.input);
    }
});

export const NoteDeleteField = mutationField("deleteNote", {
    type: DeletedInfo,
    args: {
        input: arg({
            type: deleteNoteInputType,
            required: true,
        }),
    },
    async resolve (root, args, context) {
        return await NotesRepository.delete(context, args.input.id);
    }
});
exports.up = function(knex) {
    knex.schema.hasTable('notes').then(async exists => {
        if(!exists) {
            await knex.schema.createTable('notes', function (t) {
                t.uuid('id').notNullable();
                t.timestamps(false, true);
                t.text('content');
            });
        }
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('notes');
};

import { makeSchema } from "nexus";
import * as Schema from './schema';

export const schema = makeSchema({
    types: Schema,
    outputs: {
        schema: __dirname + "/generated/schema.graphql",
        typegen: __dirname + "/generated/typings.ts",
    },
});

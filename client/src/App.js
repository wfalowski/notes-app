import React from 'react';
import './App.css';
import List from "./pages/List";
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import { Router, Route } from 'react-router-dom';

import { createBrowserHistory } from 'history';
import Detail from "./pages/Detail";

const client = new ApolloClient({
    uri: 'http://localhost:4000/',
});

const history = createBrowserHistory();

function App() {
    return (
        <ApolloProvider client={ client }>
            <Router history={history}>
                <div className="App">
                    <header className="App-header">
                        Notes App
                    </header>
                    <Route path="/" component={List} exact/>
                    <Route path="/:id" component={Detail} />
                </div>
            </Router>
        </ApolloProvider>
    );
}

export default App;

import React, { useState } from 'react';
import { debounce } from 'lodash';
import ReactMde from "react-mde"
import Button from "@material-ui/core/Button";

import "react-mde/lib/styles/css/react-mde-all.css";

import { markdownConverter } from "../../utils";

const Editor = ({ onAddNoteClick }) => {
    const [content, setContent] = useState("");
    const [selectedTab, setSelectedTab] = useState("write");

    const debouncedAddNoteClick = debounce(onAddNoteClick, 250);

    const handleAddNoteClick = () => {
       debouncedAddNoteClick(content)
    };

    return (
        <div style={{ maxWidth: 800, marginBottom: 50 }}>
            <ReactMde
                value={content}
                onChange={setContent}
                selectedTab={selectedTab}
                onTabChange={setSelectedTab}
                generateMarkdownPreview={markdown =>
                    Promise.resolve(markdownConverter.makeHtml(markdown))
                }
            />
            <Button style={{ marginTop: 20 }} variant="contained" onClick={handleAddNoteClick}>Save note</Button>
        </div>
    );
};

export default Editor;

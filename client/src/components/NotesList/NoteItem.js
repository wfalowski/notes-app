import React from 'react';
import Button from "@material-ui/core/Button";
import { Link } from 'react-router-dom';
import { markdownConverter } from "../../utils";

const NoteItem = ({ id, content, createdAt, onDeleteClick }) => {
    return (
        <div style={{ display: 'flex', flex: 1, justifyContent: 'space-between' }}>
            <div>
                <Link to={`/${id}`}>
                    <p className="content" dangerouslySetInnerHTML={{ __html: markdownConverter.makeHtml(content) }} />
                </Link>
                <p style={{ marginTop: 40 }}>{ (new Date(parseInt(createdAt))).toISOString() }</p>
            </div>
            <div style={{ margin: 'auto 0'}}>
                <Button variant="contained" onClick={() => onDeleteClick(id)}>Delete</Button>
            </div>
        </div>
    );
};

export default NoteItem;
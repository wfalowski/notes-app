import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import NoteItem from "./NoteItem";

const NotesList = ({ notes = [], onDeleteNoteClick }) => {
    return (
        <List>
            {
                notes.map((note) => (
                    <ListItem style={{width: '100%'}} divider>
                        <NoteItem { ...note } onDeleteClick={ onDeleteNoteClick }/>
                    </ListItem>
                ))
            }
        </List>
    );
};

export default NotesList;
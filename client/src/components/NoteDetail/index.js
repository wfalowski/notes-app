import React from 'react';
import { markdownConverter } from "../../utils";

const NoteDetail = ({ content, createdAt }) => {
    return (
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <div>
                <p className="content" dangerouslySetInnerHTML={{ __html: markdownConverter.makeHtml(content) }} />
                <p style={{ marginTop: 40 }}>{ (new Date(parseInt(createdAt))).toISOString() }</p>
            </div>
        </div>
    );
};

export default NoteDetail;
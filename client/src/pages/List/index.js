import React, { useEffect, useState } from 'react';
import { useMutation, useQuery } from '@apollo/react-hooks';
import Typography from "@material-ui/core/Typography";
import { gql } from 'apollo-boost'

import Editor from "../../components/Editor";
import { NotesList } from "../../components/NotesList";

const addNoteMutation = gql`
    mutation insertNote($input: InsertNoteInput!) {
        createNote(input: $input) {
            id
            content
            createdAt
            updatedAt
       }
    }
`;

const deleteNoteMutation = gql`
    mutation deleteNote($input: DeleteNoteInput!) {
        deleteNote(input: $input) {
            deleted
            id
        }
    }
`;

const notesListQuery = gql`
    {
        notes {
            nodes {
                id
                content
                createdAt
            }
        }
    }
`;

const List = () => {
    const { data } = useQuery(notesListQuery);
    const [notesList, setNotesList] = useState([]);
    const [addNote, { data: newNoteData }] = useMutation(addNoteMutation);
    const [deleteNote, { data: deleteNoteData }] = useMutation(deleteNoteMutation);

    const handleAddNote = (content) => {
        addNote({ variables: { input: { content }}})
    };

    const handleDeleteNote = (id) => {
        deleteNote({ variables: { input: { id }}})
    };

    useEffect(() => {
        if (data?.notes?.nodes) {
            const sortByCreateDate = (a, b) => b.createdAt - a.createdAt;
            setNotesList(data.notes.nodes.sort(sortByCreateDate));
        }
    }, [data]);

    useEffect(() => {
        if (newNoteData) {
            setNotesList([newNoteData.createNote, ...notesList]);
        }
    }, [newNoteData]);

    useEffect(() => {
        if (deleteNoteData) {
            setNotesList(notesList.filter(({ id }) => id !== deleteNoteData.deleteNote.id));
        }
    }, [deleteNoteData]);

    return (
        <div style={{ margin: '40px auto 0', maxWidth: 800}}>
            <Editor onAddNoteClick={handleAddNote}/>
            <Typography variant="h4">Latest notes</Typography>
            { data && <NotesList notes={notesList} onDeleteNoteClick={handleDeleteNote}/> }
        </div>
    );
};

export default List;
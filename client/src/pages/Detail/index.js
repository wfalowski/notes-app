import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import Typography from "@material-ui/core/Typography";
import { gql } from 'apollo-boost';
import { useParams, useHistory } from "react-router-dom";

import NoteDetail from "../../components/NoteDetail";
import Button from "@material-ui/core/Button";

const notesListQuery = gql`    
    query getNote($id: String!) {
        note(id: $id) {
            id
            content
            createdAt
        }
    }
`;

const Detail = () => {
    const { id } = useParams();
    const { push } = useHistory();
    const { data, error, loading } = useQuery(notesListQuery, {
        variables: { id },
    });

    const handleGoBack = () => {
        push('/');
    };

    return (
        <div style={{ margin: '40px auto 0', maxWidth: 800}}>
            <Button variant="contained" onClick={handleGoBack}>Go back</Button>
            <Typography variant="h4" style={{ marginTop: 40 }}>Single note</Typography>
            { error && 'Note not found' }
            { loading && 'Loading...' }
            { data && <NoteDetail {...data.note} /> }
        </div>
    );
};

export default Detail;